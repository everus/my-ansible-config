# Playbooks for development environment installation

1. devtools.yml

* Installs git
* Installs text editors and needed plugins for php development
* Installs php with modules

2. install_mysql.yml

* Installs and setups mysql server

3. install_httpd.yml

* Installs and setups apache2 server